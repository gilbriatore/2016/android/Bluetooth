package br.edu.up.bluetooth1;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.Set;

public class MainActivity extends AppCompatActivity {

  private TextView txtStatus;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    txtStatus = (TextView) findViewById(R.id.txtStatus);
  }

  public void getStatus(View v){
    BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
    if (ba != null){

      if (!ba.isEnabled()){
        txtStatus.setText("Ativado");
        Intent intentAtivar = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intentAtivar, 0);
        //ba.enable();
      } else {
        txtStatus.setText("Desativado");
        ba.disable();
      }
    }
  }

  public void getDevices(View v){
    BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
    Set<BluetoothDevice> pairedDevices = ba.getBondedDevices();
    // If there are paired devices
    if (pairedDevices.size() > 0) {
      // Loop through paired devices
      for (BluetoothDevice device : pairedDevices) {
        // Add the name and address to an array adapter to show in a ListView
        //mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
      }
    }
  }
}
